package com.humanoo.catfacts.view.viewholder

import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette
import com.humanoo.catfacts.R
import com.humanoo.catfacts.models.entity.Cat
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import kotlinx.android.synthetic.main.item_fact.view.*


class CatListViewHolder(view: View, private val delegate: Delegate)
    : BaseViewHolder(view) {

    interface Delegate {
        fun onItemClick(cat: Cat)
        fun onCatFactToggleLiked(cat: Cat)
    }

    private lateinit var cat: Cat

    @Throws(Exception::class)
    override fun bindData(data: Any) {
        if (data is Cat) {
            itemView.iv_like.setOnClickListener(this)
            cat = data
            drawItem()
        }
    }

    private fun drawItem() {
        itemView.run {
            item_poster_title.text = cat.text
            updateLikeImageView(itemView.iv_like)
            cat.image?.let {
                Glide.with(context)
                        .load(ContextCompat.getDrawable(itemView.context, R.drawable.ic_cat))
                        .listener(GlidePalette.with(cat.image)
                                .use(BitmapPalette.Profile.VIBRANT)
                                .intoBackground(item_poster_palette)
                                .crossfade(true))
                        .into(item_poster_post)
            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.iv_like -> toggleCatFact()
            else -> delegate.onItemClick(cat)
        }
    }

    private fun updateLikeImageView(v: View) {
        if (cat.liked!!) {
            (v as ImageView).setColorFilter(ContextCompat.getColor(v.context, R.color.liked_color), android.graphics.PorterDuff.Mode.SRC_IN)
        } else {
            (v as ImageView).setColorFilter(ContextCompat.getColor(v.context, R.color.un_liked_color), android.graphics.PorterDuff.Mode.SRC_IN)
        }
    }

    private fun toggleCatFact() {
        cat.liked = !cat.liked!!
        delegate.onCatFactToggleLiked(cat)
    }

    override fun onLongClick(v: View?) = false
}
