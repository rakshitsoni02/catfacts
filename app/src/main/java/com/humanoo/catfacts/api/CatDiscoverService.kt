package com.humanoo.catfacts.api

import androidx.lifecycle.LiveData
import com.humanoo.catfacts.models.entity.Cat
import retrofit2.http.GET
import retrofit2.http.Query


interface CatDiscoverService {
    /**
     *  Discover catfacts by different types of data like average animaltype, limit
     *  You can get a valid list of certifications from the  method.
     *
     *  @param [amount] Specify the no of results to query.
     *
     *  @return [List<Cat>] response
     */
    @GET("/facts/random/")
    fun fetchCatFacts(@Query("animal_type") animalType: String,
                      @Query("amount") amount: Int): LiveData<ApiResponse<List<Cat>>>


}
