package com.humanoo.catfacts.api.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.humanoo.catfacts.api.CatDiscoverService
import com.humanoo.catfacts.api.api.ApiUtil
import com.humanoo.catfacts.models.Resource
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.repository.FactsRepository
import com.humanoo.catfacts.room.CatDao
import com.humanoo.catfacts.utils.MockTestUtil.Companion.mockFact
import com.humanoo.catfacts.view.ui.main.MainActivityViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MainActivityViewModelTest {

    private lateinit var viewModel: MainActivityViewModel

    private lateinit var factsRepository: FactsRepository

    private val catDao = mock<CatDao>()

    private val discoverService = mock<CatDiscoverService>()

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        factsRepository = FactsRepository(discoverService, catDao)
        viewModel = MainActivityViewModel(factsRepository)
    }

    @Test
    fun loadFactList() {
        val loadFromDB = MutableLiveData<List<Cat>>()
        whenever(catDao.getCatList(1)).thenReturn(loadFromDB)
        var list: List<Cat> = emptyList()
        val call = ApiUtil.successCall(list)
        whenever(discoverService.fetchCatFacts("cat", 20)).thenReturn(call)

        val data = viewModel.getFactsListObservable()
        val observer = mock<Observer<Resource<List<Cat>>>>()
        data.observeForever(observer)

        viewModel.postFactsPage(1)
        verify(catDao).getCatList(1)
        verifyNoMoreInteractions(discoverService)

        val mockFactList = ArrayList<Cat>()
        mockFactList.add(mockFact())
        loadFromDB.postValue(mockFactList)
        verify(observer).onChanged(
                Resource.success(viewModel.getFactsListValues()!!.data, false)
        )
    }
}
