# CatFacts

## Development process
Based on Test-driven development.<br>
1. API Service -> API Service Unit Test with api response mock files
2. DAO -> DAO Unit Test
3. Repository -> Repository Unit Test
4. ViewModel -> ViewModel Unit Test
5. DI & Refactoring
6. Implmentating UI & Layouts <br><br>


## Build Problems
if R file not resolve to a variable without any error in build then
please make sure you running on latest android studio because new gradle plugin may misbehave in old IDE

## API
CatFacts documentation available at https://alexwohlbruck.github.io/cat-facts/docs/
`/facts` may not give result so use with other params from docs
`Pagination` not implemented properly because API not providing page info in result
 also LikedFacts page is also commented for the same

# License
```xml
The MIT License (MIT)

Copyright (c) 2019 Rax

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
