package com.humanoo.catfacts.view.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.humanoo.catfacts.models.Resource
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.repository.FactsRepository
import com.humanoo.catfacts.utils.AbsentLiveData
import timber.log.Timber
import javax.inject.Inject


class MainActivityViewModel @Inject
constructor(private val factsRepository: FactsRepository)
    : ViewModel() {

    private var factsPageLiveData: MutableLiveData<Int> = MutableLiveData()
    private val catListLiveData: LiveData<Resource<List<Cat>>>

    //  private var likedFactsLiveData: MutableLiveData<Int> = MutableLiveData()
//  private val likedFactListLiveData: LiveData<Resource<List<Cat>>>
    init {
        Timber.d("injection MainActivityViewModel")

        catListLiveData = Transformations.switchMap(factsPageLiveData) {
            factsPageLiveData.value?.let { factsRepository.loadCatFacts(it) }
                    ?: AbsentLiveData.create()
        }

//    likedFactListLiveData=Transformations.switchMap(likedFactsLiveData) {
//        likedFactsLiveData.value?.let { factsRepository.loadLikedCatFacts(it) }
//              ?: AbsentLiveData.create()
//    }
    }

    fun getFactsListObservable() = catListLiveData
    fun getFactsListValues() = getFactsListObservable().value
    fun postFactsPage(page: Int) = factsPageLiveData.postValue(page)
    fun updateLikeStatus(cat: Cat) = factsRepository.catDao.updateCat(cat)

//  fun getLikedListObservable() = likedFactListLiveData
//  fun getLikedListValues() = getLikedListObservable().value
//  fun postLikedPage(page: Int) = likedFactsLiveData.postValue(page)

}
