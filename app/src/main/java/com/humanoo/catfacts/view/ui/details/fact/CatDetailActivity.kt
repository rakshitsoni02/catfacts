package com.humanoo.catfacts.view.ui.details.fact

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.humanoo.catfacts.R
import com.humanoo.catfacts.extension.applyToolbarMargin
import com.humanoo.catfacts.extension.requestGlideListener
import com.humanoo.catfacts.extension.simpleToolbarWithHome
import com.humanoo.catfacts.models.entity.Cat
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_fact_detail.*
import kotlinx.android.synthetic.main.layout_detail_body.*
import kotlinx.android.synthetic.main.layout_detail_header.*
import javax.inject.Inject


class CatDetailActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(MovieDetailViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fact_detail)
        initializeUI()
    }

    @SuppressLint("SetTextI18n")
    private fun initializeUI() {
        applyToolbarMargin(movie_detail_toolbar)
        simpleToolbarWithHome(movie_detail_toolbar, "CatFact Detail")
        Glide.with(this).load(ContextCompat.getDrawable(this, R.drawable.ic_cat))
                .listener(requestGlideListener(movie_detail_poster))
                .into(movie_detail_poster)
        detail_header_title.text = getCatFactFromIntent().text
        detail_header_release.text = "Created : ${getCatFactFromIntent().createdAt}"
        detail_body_summary.text = getCatFactFromIntent().text!!
    }

    private fun getCatFactFromIntent(): Cat {
        return intent.getParcelableExtra("cat") as Cat
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return false
    }

}
