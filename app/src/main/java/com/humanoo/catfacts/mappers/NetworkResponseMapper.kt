package com.humanoo.catfacts.mappers

import com.humanoo.catfacts.models.NetworkResponseModel


interface NetworkResponseMapper<in FROM : List<NetworkResponseModel>> {
    fun onLastPage(response: FROM): Boolean
}
