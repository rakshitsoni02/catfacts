package com.humanoo.catfacts.api.api

import com.humanoo.catfacts.api.CatDiscoverService
import com.humanoo.catfacts.utils.LiveDataTestUtil
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import java.io.IOException



class CatDiscoverServiceTest : ApiAbstract<CatDiscoverService>() {

  private lateinit var service: CatDiscoverService

  @Before
  fun initService() {
    this.service = createService(CatDiscoverService::class.java)
  }

  @Throws(IOException::class)
  @Test
  fun fetchCatFactListTest() {
    enqueueResponse("/cat_facts.json")
    val response = LiveDataTestUtil.getValue(service.fetchCatFacts("cat",20))
    assertThat(response.body?.get(0)?._id, `is`("591f9858c5cbe314f7a7ad36"))
  }

}
