package com.humanoo.catfacts.di

import com.humanoo.catfacts.view.ui.details.fact.CatDetailActivity
import com.humanoo.catfacts.view.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Suppress("unused")
@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeFactDetailActivity(): CatDetailActivity
}
