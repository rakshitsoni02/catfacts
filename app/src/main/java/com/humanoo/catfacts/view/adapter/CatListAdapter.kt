package com.humanoo.catfacts.view.adapter

import android.view.View
import com.skydoves.baserecyclerviewadapter.BaseAdapter
import com.skydoves.baserecyclerviewadapter.BaseViewHolder
import com.skydoves.baserecyclerviewadapter.SectionRow
import com.humanoo.catfacts.R
import com.humanoo.catfacts.models.Resource
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.view.viewholder.CatListViewHolder


class CatListAdapter(private val delegate: CatListViewHolder.Delegate)
    : BaseAdapter() {

    init {
        addSection(ArrayList<Cat>())
    }

    fun addCatList(resource: Resource<List<Cat>>) {
        resource.data?.let {
            sections()[0].addAll(it)
            notifyDataSetChanged()
        }
    }

    override fun layout(sectionRow: SectionRow): Int {
        return R.layout.item_fact
    }

    override fun viewHolder(layout: Int, view: View): BaseViewHolder {
        return CatListViewHolder(view, delegate)
    }
}
