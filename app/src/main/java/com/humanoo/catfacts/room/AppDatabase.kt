package com.humanoo.catfacts.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.utils.StringListConverter


@Database(entities = [(Cat::class)],
        version = 3, exportSchema = false)
@TypeConverters(value = [(StringListConverter::class)])
abstract class AppDatabase : RoomDatabase() {
    abstract fun catDao(): CatDao
}
