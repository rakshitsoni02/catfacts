package com.humanoo.catfacts.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.humanoo.catfacts.models.entity.Cat


@Dao
interface CatDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCatList(cats: List<Cat>)

    @Update
    fun updateCat(cat: Cat)

    @Query("SELECT * FROM Cat WHERE _id = :id_")
    fun getCat(id_: String): Cat

    @Query("SELECT * FROM Cat WHERE liked = :value")
    fun getLikedFacts(value: Boolean): LiveData<List<Cat>>

    @Query("SELECT * FROM Cat WHERE page = :page_")
    fun getCatList(page_: Int): LiveData<List<Cat>>
}
