package com.humanoo.catfacts.view.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class MainPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return CatFactListFragment()
            1 -> return LikedListFragment()
            else -> return CatFactListFragment()
        }
    }

    override fun getCount() = 2
}
