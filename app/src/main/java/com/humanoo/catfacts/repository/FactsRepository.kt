package com.humanoo.catfacts.repository

import androidx.lifecycle.LiveData
import com.humanoo.catfacts.api.Api
import com.humanoo.catfacts.api.ApiResponse
import com.humanoo.catfacts.api.CatDiscoverService
import com.humanoo.catfacts.mappers.CatResponseMapper
import com.humanoo.catfacts.models.Resource
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.room.CatDao
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class FactsRepository @Inject
constructor(
        val discoverService: CatDiscoverService,
        val catDao: CatDao
) : Repository {

    init {
        Timber.d("Injection FactsRepository")
    }

    fun loadCatFacts(page: Int): LiveData<Resource<List<Cat>>> {
        return object : NetworkBoundRepository<List<Cat>, List<Cat>, CatResponseMapper>() {
            override fun saveFetchData(items: List<Cat>) {
                for (item in items) {
                    item.page = page
                    item.liked = false
                    item.image = Api.getDefaultPath()
                }
                catDao.insertCatList(cats = items)
            }

            override fun shouldFetch(data: List<Cat>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun loadFromDb(): LiveData<List<Cat>> {
                return catDao.getCatList(page_ = page)
            }

            override fun fetchService(): LiveData<ApiResponse<List<Cat>>> {
                return discoverService.fetchCatFacts("cat", 20)
            }

            override fun mapper(): CatResponseMapper {
                return CatResponseMapper()
            }

            override fun onFetchFailed(message: String?) {
                Timber.d("onFetchFailed $message")
            }
        }.asLiveData()
    }

}
