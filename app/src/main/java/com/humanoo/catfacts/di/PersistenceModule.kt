package com.humanoo.catfacts.di

import android.app.Application
import androidx.annotation.NonNull
import androidx.room.Room
import com.humanoo.catfacts.room.AppDatabase
import com.humanoo.catfacts.room.CatDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class PersistenceModule {

    @Provides
    @Singleton
    fun provideDatabase(@NonNull application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "TheCats.db").allowMainThreadQueries().build()
    }

    @Provides
    @Singleton
    fun provideCatDao(@NonNull database: AppDatabase): CatDao {
        return database.catDao()
    }
}
