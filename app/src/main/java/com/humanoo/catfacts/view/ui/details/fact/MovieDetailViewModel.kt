package com.humanoo.catfacts.view.ui.details.fact

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.humanoo.catfacts.repository.FactsRepository
import timber.log.Timber
import javax.inject.Inject


class MovieDetailViewModel @Inject
constructor(private val repository: FactsRepository) : ViewModel() {

    private val keywordIdLiveData: MutableLiveData<Int> = MutableLiveData()

    private val videoIdLiveData: MutableLiveData<Int> = MutableLiveData()

    private val reviewIdLiveData: MutableLiveData<Int> = MutableLiveData()

    init {
        Timber.d("Injection FactDetailViewModel")


        fun postKeywordId(id: Int) = keywordIdLiveData.postValue(id)

        fun postVideoId(id: Int) = videoIdLiveData.postValue(id)

        fun postReviewId(id: Int) = reviewIdLiveData.postValue(id)
    }
}
