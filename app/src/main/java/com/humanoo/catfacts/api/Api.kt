package com.humanoo.catfacts.api


object Api {
    private const val BASE_POSTER_PATH = "https://image.tmdb.org/t/p/w342"
    private const val BASE_BACKDROP_PATH = "https://image.tmdb.org/t/p/w780"
    private const val YOUTUBE_VIDEO_URL = "https://www.youtube.com/watch?v="
    private const val YOUTUBE_THUMBNAIL_URL = "https://img.youtube.com/vi/"
    private const val DEFAULT_IMAGE = "https://upload.wikimedia.org/wikipedia/commons/6/69/June_odd-eyed-cat_cropped.jpg"

    fun getPosterPath(posterPath: String): String {
        return Api.BASE_POSTER_PATH + posterPath
    }

    fun getDefaultPath(): String {
        return Api.DEFAULT_IMAGE
    }

    fun getBackdropPath(backdropPath: String): String {
        return Api.BASE_BACKDROP_PATH + backdropPath
    }

    fun getYoutubeVideoPath(videoPath: String): String {
        return Api.YOUTUBE_VIDEO_URL + videoPath
    }

    fun getYoutubeThumbnailPath(thumbnailPath: String): String {
        return "${Api.YOUTUBE_THUMBNAIL_URL} + $thumbnailPath + /default.jpg"
    }
}
