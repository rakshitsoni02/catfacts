package com.humanoo.catfacts.api.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.humanoo.catfacts.api.CatDiscoverService
import com.humanoo.catfacts.api.api.ApiUtil.successCall
import com.humanoo.catfacts.models.Resource
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.repository.FactsRepository
import com.humanoo.catfacts.room.CatDao
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class FactsRepositoryTest {

    private lateinit var repository: FactsRepository
    private val catDao = mock<CatDao>()
    private val service = mock<CatDiscoverService>()

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        repository = FactsRepository(service, catDao)
    }

    @Test
    fun loadFactListFromNetwork() {
        val loadFromDB = MutableLiveData<List<Cat>>()
        whenever(catDao.getCatList(1)).thenReturn(loadFromDB)
        var list: List<Cat> = emptyList()
        val call = successCall(list)
        whenever(service.fetchCatFacts("cat", 20)).thenReturn(call)

        val data = repository.loadCatFacts(1)
        verify(catDao).getCatList(1)
        verifyNoMoreInteractions(service)

        val observer = mock<Observer<Resource<List<Cat>>>>()
        data.observeForever(observer)
        verifyNoMoreInteractions(service)
        val updatedData = MutableLiveData<List<Cat>>()
        whenever(catDao.getCatList(1)).thenReturn(updatedData)

        loadFromDB.postValue(null)
        verify(observer).onChanged(Resource.loading(null))
        verify(service).fetchCatFacts("cat", 20)
        verify(catDao).insertCatList(list)

        updatedData.postValue(list)
        verify(observer).onChanged(Resource.success(list, false))
    }

}
