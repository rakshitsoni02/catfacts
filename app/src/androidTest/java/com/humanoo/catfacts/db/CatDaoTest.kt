package com.humanoo.catfacts.db

import androidx.test.runner.AndroidJUnit4
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.utils.LiveDataTestUtil
import com.humanoo.catfacts.utils.MockTestUtil.Companion.mockFact
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith



@RunWith(AndroidJUnit4::class)
class CatDaoTest : DbTest() {

  @Test
  fun insertAndReadTest() {
    val cats = ArrayList<Cat>()
    val cat = mockFact()
    cats.add(cat)

    db.catDao().insertCatList(cats)
    val loadFromDB = LiveDataTestUtil.getValue(db.catDao().getCatList(cat.page))[0]
    assertThat(loadFromDB.page, `is`(1))
    assertThat(loadFromDB._id, `is`("123"))
  }

  @Test
  fun updateAndReadTest() {
    val cats = ArrayList<Cat>()
    val cat = mockFact()
    cats.add(cat)
    db.catDao().insertCatList(cats)

    val loadFromDB = db.catDao().getCat(cat._id)
    assertThat(loadFromDB.page, `is`(1))

    cat.page = 10
    db.catDao().updateCat(cat)

    val updated = db.catDao().getCat(cat._id)
    assertThat(updated.page, `is`(10))
  }
}
