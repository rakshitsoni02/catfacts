package com.humanoo.catfacts.mappers

import com.humanoo.catfacts.models.entity.Cat


class CatResponseMapper : NetworkResponseMapper<List<Cat>> {
    override fun onLastPage(response: List<Cat>): Boolean {
//    Timber.d("loadPage : ${response.page}/${response.total_pages}")
//    return response.page > response.total_pages
//        return response[0].page > 25
        return true
    }
}
