package com.humanoo.catfacts.view.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.skydoves.baserecyclerviewadapter.RecyclerViewPaginator
import com.humanoo.catfacts.R
import com.humanoo.catfacts.models.Resource
import com.humanoo.catfacts.models.Status
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.view.adapter.CatListAdapter
import com.humanoo.catfacts.view.ui.details.fact.CatDetailActivity
import com.humanoo.catfacts.view.viewholder.CatListViewHolder
import dagger.android.support.AndroidSupportInjection
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject


@Suppress("SpellCheckingInspection")
class LikedListFragment : Fragment(), CatListViewHolder.Delegate {
    override fun onCatFactToggleLiked(cat: Cat) {

    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainActivityViewModel

    private val adapter = CatListAdapter(this)
    private lateinit var paginator: RecyclerViewPaginator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_fragment_cat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeUI()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)
        observeViewModel()
    }

    private fun initializeUI() {
//    recyclerView.adapter = adapter
//    recyclerView.layoutManager = GridLayoutManager(context, 1)
//    paginator = RecyclerViewPaginator(
//        recyclerView = recyclerView,
//        isLoading = { viewModel.getLikedListValues()?.status == Status.LOADING },
//        loadMore = { loadMore(it) },
//        onLast = { viewModel.getLikedListValues()?.onLastPage!! }
//    )
//    paginator.currentPage = 1
    }

    private fun observeViewModel() {
//    observeLiveData(viewModel.getLikedListObservable()) { updateLikedList(it) }
//    viewModel.postLikedPage(1)
    }

    private fun updateLikedList(resource: Resource<List<Cat>>) {
        when (resource.status) {
            Status.LOADING -> Unit
            Status.SUCCESS -> adapter.addCatList(resource)
            Status.ERROR -> toast(resource.errorEnvelope?.status_message.toString())
        }
    }

    private fun loadMore(page: Int) {
//    viewModel.postLikedPage(page)
    }

    override fun onItemClick(tv: Cat) {
        startActivity<CatDetailActivity>("cat" to Cat)
    }
}
