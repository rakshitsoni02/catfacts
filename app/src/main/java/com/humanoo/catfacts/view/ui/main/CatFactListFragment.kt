package com.humanoo.catfacts.view.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.humanoo.catfacts.R
import com.humanoo.catfacts.extension.observeLiveData
import com.humanoo.catfacts.models.Resource
import com.humanoo.catfacts.models.Status
import com.humanoo.catfacts.models.entity.Cat
import com.humanoo.catfacts.view.adapter.CatListAdapter
import com.humanoo.catfacts.view.ui.details.fact.CatDetailActivity
import com.humanoo.catfacts.view.viewholder.CatListViewHolder
import com.skydoves.baserecyclerviewadapter.RecyclerViewPaginator
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.main_fragment_cats.*
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject


@Suppress("SpellCheckingInspection")
class CatFactListFragment : Fragment(), CatListViewHolder.Delegate {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainActivityViewModel

    private val adapter = CatListAdapter(this)
    private lateinit var paginator: RecyclerViewPaginator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_fragment_cats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeUI()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)
        observeViewModel()
    }

    private fun initializeUI() {
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(context, 1)
        paginator = RecyclerViewPaginator(
                recyclerView = recyclerView,
                isLoading = { viewModel.getFactsListValues()?.status == Status.LOADING },
                loadMore = { loadMore(it) },
                onLast = { viewModel.getFactsListValues()?.onLastPage!! }
        )
        paginator.currentPage = 1
    }

    private fun observeViewModel() {
        observeLiveData(viewModel.getFactsListObservable()) { updateFactsList(it) }
        viewModel.postFactsPage(1)
    }

    private fun updateFactsList(resource: Resource<List<Cat>>) {
        when (resource.status) {
            Status.LOADING -> Unit
            Status.SUCCESS -> adapter.addCatList(resource)
            Status.ERROR -> toast(resource.errorEnvelope?.status_message.toString())
        }
    }

    private fun loadMore(page: Int) {
        viewModel.postFactsPage(page)
    }

    override fun onItemClick(cat: Cat) {
        startActivity<CatDetailActivity>("cat" to cat)
    }

    override fun onCatFactToggleLiked(cat: Cat) {
        viewModel.updateLikeStatus(cat)
    }
}
