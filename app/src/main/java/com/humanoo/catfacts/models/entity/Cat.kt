package com.humanoo.catfacts.models.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import com.humanoo.catfacts.models.NetworkResponseModel


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "unused")
@Entity(primaryKeys = [("_id")])
data class Cat(
        val updatedAt: String?,
        val _id: String,
        val createdAt: String?,
        val user: String?,
        val text: String?,
        val __v: Int,
        val deleted: Boolean,
        val used: Boolean,
        val type: String?,
        val source: String?,
        var page: Int,
        var image: String?,
        var liked: Boolean?

) : Parcelable, NetworkResponseModel {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readInt() == 1,
            source.readInt() == 1,
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readInt() == 1
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(updatedAt)
        writeString(_id)
        writeString(createdAt)
        writeString(user)
        writeString(text)
        writeString(type)
        writeString(image)
        writeString(source)
        writeInt(__v)
        writeInt(page)
        writeInt(if (deleted) 1 else 0)
        writeInt(if (used) 1 else 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Cat> = object : Parcelable.Creator<Cat> {
            override fun createFromParcel(source: Parcel): Cat = Cat(source)
            override fun newArray(size: Int): Array<Cat?> = arrayOfNulls(size)
        }
    }
}
