package com.humanoo.catfacts.models.network

import com.humanoo.catfacts.models.NetworkResponseModel
import com.humanoo.catfacts.models.entity.Cat

//Not useful because of no pagination
data class DiscoverCatFactResponse(
        val page: Int,
        val results: List<Cat>,
        val total_results: Int,
        val total_pages: Int
) : NetworkResponseModel
