package com.humanoo.catfacts.di

import com.humanoo.catfacts.view.ui.main.CatFactListFragment
import com.humanoo.catfacts.view.ui.main.LikedListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Suppress("unused")
@Module
abstract class MainActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeCatFactListFragment(): CatFactListFragment

    @ContributesAndroidInjector
    abstract fun contributeTvListFragment(): LikedListFragment
}
