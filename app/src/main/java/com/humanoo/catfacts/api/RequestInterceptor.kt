package com.humanoo.catfacts.api

import okhttp3.Interceptor
import okhttp3.Response


internal class RequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalUrl = originalRequest.url()
        val url = originalUrl.newBuilder()
//        .addQueryParameter("api_key", BuildConfig.FACT_API_KEY) // in case of auth
                .build()

        val requestBuilder = originalRequest.newBuilder().url(url)
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}
